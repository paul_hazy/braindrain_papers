Initialization scripts
sh - .profile
csh - .login, .cshrc
ksh - .profile, .kshrc
bash - .profile, .bash_profile, .bashrc ( инициализационном файлом является /etc/profile)

.profile, .bash_profile, .login запускаются при входе в систему, .cshrc, .kshrc, .bashrc при каждом запуске интерпретатора